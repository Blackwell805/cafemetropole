import MenuPage from "src/views/MenuPage.vue";
import AboutPage from "src/views/AboutPage.vue";


const routes = [
  {
    path: '/',
    component: () => import('layouts/MainPage.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/menu', component: () => import('src/views/MenuPage.vue')},
      { path: '/about', component: () => import('src/views/AboutPage.vue')},
      { path: '/contact', component: () => import('src/views/ContactPage.vue')},
    ]
  },
  // The pathway below can be done this way if I wanted
  // to migrate to a separate page.
  // {
  //   path: "/menu",
  //   component: () => import('src/views/MenuPage.vue'),
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
